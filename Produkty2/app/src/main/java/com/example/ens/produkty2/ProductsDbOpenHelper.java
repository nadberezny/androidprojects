package com.example.ens.produkty2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ProductsDbOpenHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private final ProductsTable productsTable;

    ProductsDbOpenHelper(Context context) {
        super(context, "products_db", null, DATABASE_VERSION);
        productsTable = new ProductsTable();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        productsTable.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i2) {
        productsTable.onUpgrade(db);
    }

    public Cursor getProduct() {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        return readableDatabase.query(ProductsTable.TABLE_NAME, null, null, null, null, null, null);
    }

    public String getProduct(long id) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Cursor cursor = readableDatabase.query(ProductsTable.TABLE_NAME, null, ProductsTable.ID + "=?", new String[]{id + ""}, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            String product = cursor.getString(cursor.getColumnIndex(ProductsTable.PRODUCT_NAME));
            cursor.close();
            return product;
        }
        return null;
    }

    public long insertProduct(String productName) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ProductsTable.PRODUCT_NAME, productName);
        return writableDatabase.insert(ProductsTable.TABLE_NAME, null, contentValues);

    }

    public int editProduct(long id, String newProduct) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ProductsTable.PRODUCT_NAME, newProduct);
        return writableDatabase.update(ProductsTable.TABLE_NAME, contentValues, ProductsTable.ID + "=?", new String[]{id + ""});
    }


    public int deleteProduct(long id) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        return writableDatabase.delete(ProductsTable.TABLE_NAME, ProductsTable.ID + "=?", new String[]{id + ""});
    }

}
