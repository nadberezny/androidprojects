package com.example.ens.produkty2;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by ENS on 2014-11-03.
 */
public class EditProduct extends Activity {

    private EditText editedText;
    private Button btnSaveChanges;
    private Button btnDeleteProduct;
    private long productIndex;
    private final String TAG = "EditProduct";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);

//        final Core application = (Core) getApplication();
        final ProductsDbOpenHelper dbOpenHelper = new ProductsDbOpenHelper(this);

        if (getIntent().hasExtra("productIndex"))
            productIndex = getIntent().getLongExtra("productIndex", -1);

        if (productIndex == -1) {
            setResult(RESULT_CANCELED);
            Log.e(TAG, "nie znalazlem indeksu dla tego elementu");
            finish();
        }

        editedText = (EditText) findViewById(R.id.edit_text);//super!
        final String textToBeEdited = dbOpenHelper.getProduct(productIndex);
        editedText.setText(textToBeEdited);

        btnSaveChanges = (Button) findViewById(R.id.btn_save_changes);
        btnSaveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                application.getArrayProductList().set(productIndex, editedText.getText().toString());
                dbOpenHelper.editProduct(productIndex, editedText.getText().toString());
                setResult(RESULT_OK);
                finish();
            }
        });

        btnDeleteProduct = (Button) findViewById(R.id.delete_product);
        btnDeleteProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbOpenHelper.deleteProduct(productIndex);
                setResult(RESULT_OK);
                finish();
            }
        });
    }
}
