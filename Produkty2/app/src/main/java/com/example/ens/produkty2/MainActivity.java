package com.example.ens.produkty2;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends Activity implements View.OnClickListener {

    private Button btnProductList;
    private Button btnInfo;
    private Button btnAuthor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnProductList = (Button) findViewById(R.id.btn_product_list);
        btnProductList.setOnClickListener(this);

        btnInfo = (Button) findViewById(R.id.btn_info);
        btnInfo.setOnClickListener(this);

        btnAuthor = (Button) findViewById(R.id.btn_author);
        btnAuthor.setOnClickListener(this);



    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch(id)
        {
            case R.id.btn_product_list:
                //startActivity(new Intent(MainActivity.this, ProductListActivity.class));
                Intent intentProductList = new Intent(this, ProductListActivity.class);
                startActivity(intentProductList);
                break;

            case R.id.btn_info:
                startActivity(new Intent(MainActivity.this, InfoActivity.class));
                break;

            case R.id.btn_author:
                startActivity(new Intent(MainActivity.this, AuthorActivity.class));
                break;
        }
    }
}
