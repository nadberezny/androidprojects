package com.example.ens.produkty2;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class AddProduct extends Activity {
    private EditText productName;
    private Button productAdd;
    private final String TAG = AddProduct.this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

//        final Core application = (Core) getApplication();
        final ProductsDbOpenHelper dbOpenHelper = new ProductsDbOpenHelper(this);

        productName = (EditText) findViewById(R.id.edit_product_name);
        productAdd = (Button) findViewById(R.id.product_add);
        productAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strProduct = productName.getText().toString();
                Intent intent = new Intent("notification_product");
                intent.putExtra("product", strProduct);
                Log.i(TAG, "sending broadcast:"+ strProduct);
                sendBroadcast(intent);

//                application.getArrayProductList().add(strProduct);
                long result = dbOpenHelper.insertProduct(strProduct);
                if (result > -1) {
                    setResult(RESULT_OK);
                    Log.i(TAG, "Wpis dodany do bazy");
                }
                else {
                    setResult(RESULT_CANCELED);
                    Log.e(TAG, "!Wpis do bazy zakończony niepowodzeniem!");
                }

                finish();
            }
        });
    }
}
