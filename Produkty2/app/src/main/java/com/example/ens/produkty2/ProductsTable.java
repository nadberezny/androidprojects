package com.example.ens.produkty2;

import android.database.sqlite.SQLiteDatabase;

public class ProductsTable {
    public static final String TABLE_NAME = "products";
    public static final String ID = "_id";
    public static final String PRODUCT_NAME = "product_name";

    private static final String PRODUCTS_TABLE_CREATE =
            "CREATE TABLE " + TABLE_NAME +
                    " ( " +
                    ID + " INTEGER PRIMARY KEY AUTOINCREMENT , " +
                    PRODUCT_NAME + " TEXT " +
                    ");";

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PRODUCTS_TABLE_CREATE);
    }

    public void onUpgrade(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL(PRODUCTS_TABLE_CREATE);
    }
}
