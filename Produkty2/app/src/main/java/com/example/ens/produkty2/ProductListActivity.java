package com.example.ens.produkty2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.zip.Inflater;


public class ProductListActivity extends Activity {

    private static final int REQUEST_ADD_PRODUCT = 0;
    private static final int REQUEST_EDIT_PRODUCT = 1;
    private ListView listView;
    private Button addProduct;
    private ProductsDbOpenHelper dbOpenHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        dbOpenHelper = new ProductsDbOpenHelper(this);

        listView = (ListView) findViewById(R.id.listView);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,core.getArrayProductList());
        CursorAdapter adapter = new CursorAdapter(this, dbOpenHelper.getProduct(), true) {
            @Override
            public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
                return LayoutInflater.from(ProductListActivity.this).inflate(android.R.layout.simple_list_item_1, viewGroup, false);
            }

            @Override
            public void bindView(View view, Context context, Cursor cursor) {
                ((TextView)view.findViewById(android.R.id.text1))
                        .setText(cursor.getString(cursor.getColumnIndex(ProductsTable.PRODUCT_NAME)));
            }
        };

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(ProductListActivity.this, EditProduct.class);
                intent.putExtra("productIndex", l);
                startActivityForResult(intent, REQUEST_EDIT_PRODUCT);
            }
        });

        addProduct = (Button) findViewById(R.id.addProductBtn);
        addProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductListActivity.this, AddProduct.class);
                startActivityForResult(intent, REQUEST_ADD_PRODUCT);
            }
        });
    }

    private String TAG = "lista produktow";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (REQUEST_ADD_PRODUCT == requestCode) {
            if (RESULT_OK == resultCode) {
//                ((ArrayAdapter<String>) listView.getAdapter()).notifyDataSetChanged();
                ((CursorAdapter) listView.getAdapter()).swapCursor(dbOpenHelper.getProduct());
            }
        }

        if (REQUEST_EDIT_PRODUCT == requestCode) {
            if (RESULT_OK == resultCode) {
//                ((ArrayAdapter<String>) listView.getAdapter()).notifyDataSetChanged();
                ((CursorAdapter) listView.getAdapter()).swapCursor(dbOpenHelper.getProduct());
            } else {
                Log.e(TAG, "edycja nie powiodla sie");
            }
        }
    }
}
