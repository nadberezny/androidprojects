package julek.julekmapa;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class PlacesList extends Activity {

private ListView listView;
private PlacesDbOpenHelper dbOpenHelper;



        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_places_list);

            dbOpenHelper = new PlacesDbOpenHelper(this);


            listView = (ListView) findViewById(R.id.listView);

            CursorAdapter adapter = new CursorAdapter(this, dbOpenHelper.getPlaces(), true) {
                @Override
                public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
                    return LayoutInflater.from(PlacesList.this).inflate(android.R.layout.simple_list_item_2, viewGroup, false);
                }

                @Override
                public void bindView(View view, Context context, Cursor cursor) {
                    ((TextView)view.findViewById(android.R.id.text2))
                            .setText(cursor.getString(cursor.getColumnIndex(PlacesTable.PLACE_NAME))
                                    + " Lat: "
                                    + cursor.getString(cursor.getColumnIndex(PlacesTable.LATITUDE))
                                    + " Long: "
                                    + cursor.getString(cursor.getColumnIndex(PlacesTable.LONGITUDE)));
                }
            };

            listView.setAdapter(adapter);
        }

}
