package julek.julekmapa;


import android.database.sqlite.SQLiteDatabase;


public class PlacesTable {

    public static final String TABLE_NAME   = "PLACES";
    public static final String ID           = "_id";
    public static final String PLACE_NAME   = "PLACE_NAME";
    public static final String LATITUDE     = "latitude";
    public static final String LONGITUDE    = "longitude";

    private static final String PLACES_TABLE_CREATE =

            "CREATE TABLE " + TABLE_NAME +
                    " ( " +
                    ID         + " INTEGER PRIMARY KEY AUTOINCREMENT , " +
                    PLACE_NAME + " TEXT , " +
                    LATITUDE   + " REAL , " +
                    LONGITUDE  + " REAL " +
                    ");";


    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PLACES_TABLE_CREATE);
    }

    public void onUpgrade(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL(PLACES_TABLE_CREATE);
    }
}