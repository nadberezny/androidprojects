package julek.julekmapa;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class AddPlace extends Activity {

    //private final String TAG = AddPlace.this.getClass().getSimpleName();

    private EditText placeName;
    private Button btnAddPlace;
    private double latitude;
    private double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_place);

        latitude = 50.214; // getIntent().getDoubleExtra("latitude_extra", 0);
        longitude = 20.968; //getIntent().getDoubleExtra("longitude_extra", 0);


        final PlacesDbOpenHelper dbOpenHelper = new PlacesDbOpenHelper(this);

        placeName = (EditText) findViewById(R.id.edit_place_name);
        btnAddPlace = (Button) findViewById(R.id.btn_save1);


        btnAddPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strPlaceName = placeName.getText().toString();

                dbOpenHelper.insertPlace(strPlaceName, latitude, longitude);

                finish();
            }
        });
    }
}
