package julek.julekmapa;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class PlacesDbOpenHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private final PlacesTable placesTable;

    PlacesDbOpenHelper(Context context) {
        super(context, "places_db", null, DATABASE_VERSION);
        placesTable = new PlacesTable();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        placesTable.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i2) {
        placesTable.onUpgrade(db);
    }

    public Cursor getPlaces() {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        return readableDatabase.query(PlacesTable.TABLE_NAME, null, null, null, null, null, null);
    }

    public String getPlaceName(long id) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Cursor cursor = readableDatabase.query(PlacesTable.TABLE_NAME, null, PlacesTable.ID + "=?", new String[]{id + ""}, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            String placeName = cursor.getString(cursor.getColumnIndex(PlacesTable.PLACE_NAME));
            cursor.close();
            return placeName;
        }
        return null;
    }

    public double getLatitude(long id) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Cursor cursor = readableDatabase.query(PlacesTable.TABLE_NAME, null, PlacesTable.ID + "=?", new String[]{id + ""}, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            double latitude = cursor.getDouble(cursor.getColumnIndex(PlacesTable.LATITUDE));
            cursor.close();
            return latitude;
        }
        return 0;
    }

    public double getLongitude(long id) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Cursor cursor = readableDatabase.query(PlacesTable.TABLE_NAME, null, PlacesTable.ID + "=?", new String[]{id + ""}, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            double longitude = cursor.getDouble(cursor.getColumnIndex(PlacesTable.LONGITUDE));
            cursor.close();
            return longitude;
        }
        return 0;
    }

    public long insertPlace(String placeName, double latitude, double longitude) {

        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(PlacesTable.PLACE_NAME, placeName);
        contentValues.put(PlacesTable.LATITUDE, latitude);
        contentValues.put(PlacesTable.LONGITUDE, longitude);

        return writableDatabase.insert(PlacesTable.TABLE_NAME, null, contentValues);
    }
}