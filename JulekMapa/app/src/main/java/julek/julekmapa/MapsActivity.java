package julek.julekmapa;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapsActivity extends FragmentActivity implements View.OnClickListener {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Button btnAddPlace;
    private Button btnMyPlaces;
    private Button btnAddMarkers;
    private Button btnRemoveMarkers;

    private PlacesDbOpenHelper dbOpenHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);


        setUpMapIfNeeded();

        btnAddPlace = (Button) findViewById(R.id.btn_add_place);
        btnAddPlace.setOnClickListener(this);

        btnMyPlaces = (Button) findViewById(R.id.btn_my_places);
        btnMyPlaces.setOnClickListener(this);

        btnAddMarkers = (Button) findViewById(R.id.btn_add_markers);
        btnAddMarkers.setOnClickListener(this);

        btnRemoveMarkers = (Button) findViewById(R.id.btn_remoove_markers);
        btnRemoveMarkers.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }

        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        Location myLocation = locationManager.getLastKnownLocation(provider);

        double latitude = myLocation.getLatitude();
        double longitude = myLocation.getLongitude();

        int id = view.getId();
        switch (id) {
            case R.id.btn_add_place:
                Intent intent = new Intent(MapsActivity.this, AddPlace.class);
                intent.putExtra("latitude_extra", latitude);
                intent.putExtra("longitude_extra", longitude);
                startActivity(intent);
                break;

            case R.id.btn_my_places:
                startActivity(new Intent(MapsActivity.this, PlacesList.class));
                break;

            case R.id.btn_add_markers:
                dbOpenHelper = new PlacesDbOpenHelper(this);


                Cursor cursor = dbOpenHelper.getPlaces();
                int countValue = cursor.getCount();

                if (countValue > 0) {
                    for (int i = 1; i < countValue + 1; i++) {

                        mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(dbOpenHelper.getLatitude(i), dbOpenHelper.getLongitude(i)))
                                    .title(dbOpenHelper.getPlaceName(i))
                        );
                    }
                }
                break;

            case R.id.btn_remoove_markers:
                mMap.clear();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {

        mMap.setMyLocationEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }

        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        Location myLocation = locationManager.getLastKnownLocation(provider);

        double latitude = myLocation.getLatitude();
        double longitude = myLocation.getLongitude();

        LatLng latLng = new LatLng(latitude, longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(14));
    }
}
