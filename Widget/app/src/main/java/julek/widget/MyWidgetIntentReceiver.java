package julek.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.widget.RemoteViews;

/**
 * Created by air on 31/01/15.
 */

public class MyWidgetIntentReceiver extends BroadcastReceiver {

    private static int clickCount = 0;

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("intent.action.ZMIEN_OBRAZ")){
            updateWidgetPictureAndButtonListener(context);
        }
        if(intent.getAction().equals("intent.action.ODTWORZ_MUZYKE")){
            updateWidgetMusicButtonListener(context);
        }
        if(intent.getAction().equals("intent.action.GOOGLE")){
            Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
            myWebLink.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            myWebLink.setData(Uri.parse("http://google.com"));
            context.startActivity(myWebLink);
        }
    }

    private void updateWidgetPictureAndButtonListener(Context context) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
        remoteViews.setImageViewResource(R.id.widget_image, getImageToSet());

        remoteViews.setOnClickPendingIntent(R.id.widget_button, MyWidgetProvider.buildButtonPendingIntent(context));
        MyWidgetProvider.pushWidgetUpdate(context.getApplicationContext(), remoteViews);


    }
    private void updateWidgetMusicButtonListener(Context context) {
        MediaPlayer mPlay = MediaPlayer.create(context, R.raw.klarnet);
        mPlay.start();

    }

    private int getImageToSet() {
        clickCount++;
        return clickCount % 2 == 0 ? R.drawable.image1 : R.drawable.image2;
    }
}
