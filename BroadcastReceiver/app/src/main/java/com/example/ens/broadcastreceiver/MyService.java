package com.example.ens.broadcastreceiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

/**
 * Created by ENS on 2014-11-17.
 */
public class MyService extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private int idNotify = 0;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

                String text = intent.getStringExtra("product");
                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                notificationManager.notify(idNotify++, generateNotofication(text));
        }
    };

    private Notification generateNotofication(String text) {

                Notification.Builder builder = new Notification.Builder(this)
                .setContentTitle("Dodano produkt")
                .setContentText(text)
                .setTicker("Dodano produkt: " + text)
                .setSmallIcon(android.R.drawable.ic_dialog_info);


        Notification notification = builder.build();
        return notification;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        registerReceiver(broadcastReceiver, new IntentFilter("notification_product"));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}
