package julek.produktygae;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.google.api.client.json.jackson2.JacksonFactory;

import java.io.IOException;

import julek.produkty4.backend.productApi.ProductApi;
import julek.produkty4.backend.productApi.model.Product;


public class EditProductActivity extends Activity {

    private Long productId;
    private String productName;
    private EditText editText;
    private Button btnSave;
    private Button btnDelete;
    private int actionType = - 1; // 1 SAVE; 2 DELETE
    private final String TAG = EditProductActivity.this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);

        productId = getIntent().getLongExtra("product_id", -1);
        productName = getIntent().getStringExtra("product_name");
        Log.i(TAG, String.valueOf(productId) + " " + productName);

        editText = (EditText) findViewById(R.id.edit_text_edit);
        editText.setText(productName);


        btnSave = (Button) findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new EditProductAsyncTask().execute();
                finish();
            }
        });

        btnSave = (Button) findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new EditProductAsyncTask().execute();
                actionType = 1;
                finish();
            }
        });

        btnDelete = (Button) findViewById(R.id.btn_delete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new EditProductAsyncTask().execute();
                actionType = 2;
                finish();
            }
        });
    }


    private class EditProductAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            Product product = new Product();
            product.setName(editText.getText().toString());

            ProductApi.Builder builder = new ProductApi.Builder(
                    AndroidHttp.newCompatibleTransport(), new JacksonFactory(), null);

            builder = CloudEndpointUtils.updateBuilder(builder);

            ProductApi endpoint = builder.build();


            switch (actionType) {

                case 1 :
                    try {
                        endpoint.update(productId, product);
                        Log.i(TAG, "zapisuję: " + productId + product);
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.i(TAG, "zapis nieudany: " + productId + product);
                    }
                    break;

                case 2 :
                    try {
                        endpoint.remove(productId);
                        Log.i(TAG, "usuwam: " + productId);
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.i(TAG, "usuwanie nieudane: " + productId);
                    }
                    break;
            }

            return null;
        }
    }

}
