package julek.produktygae;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.google.api.client.json.jackson2.JacksonFactory;

import java.io.IOException;


import julek.produkty4.backend.productApi.ProductApi;
import julek.produkty4.backend.productApi.model.Product;


public class AddProductActivity extends Activity {

    EditText editProductName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        editProductName = (EditText) findViewById(R.id.edit_product_name);

        Button btnAdd = (Button)findViewById(R.id.btn_add_product);
        btnAdd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new AddProductAsyncTask().execute();
                finish();
            }

        });
    }

    private class AddProductAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            Product product = new Product();
            product.setName(editProductName.getText().toString());


            ProductApi.Builder builder = new ProductApi.Builder(
                    AndroidHttp.newCompatibleTransport(), new JacksonFactory(), null);
            builder = CloudEndpointUtils.updateBuilder(builder);

            ProductApi endpoint = builder.build();


            try {
                endpoint.insert(product).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

    }

}
