package julek.produktygae;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.google.api.client.json.jackson2.JacksonFactory;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import julek.produkty4.backend.productApi.ProductApi;
import julek.produkty4.backend.productApi.model.CollectionResponseProduct;
import julek.produkty4.backend.productApi.model.Product;

public class ProductListActivity extends Activity {

    ListView productList = null;
    ArrayList<Map<String, String>> list = new ArrayList<>();
    private SimpleAdapter adapter;
    private String[] from = {"", "name"};
    private int[] to = {android.R.id.text1, android.R.id.text2};
    private final String TAG = ProductListActivity.this.getClass().getSimpleName();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        productList = (ListView) findViewById(R.id.listView);

        new ProductsListAsyncTask().execute();
        new ProductsListAsyncTask().onPostExecute();

        adapter = new SimpleAdapter(ProductListActivity.this, list, android.R.layout.simple_list_item_2, from, to);
        productList.setAdapter(adapter);

    }


    private class ProductsListAsyncTask extends AsyncTask<Void, Void, CollectionResponseProduct> {

        @Override
        protected CollectionResponseProduct doInBackground(Void... params) {

            ProductApi.Builder builder = new ProductApi.Builder(
                    AndroidHttp.newCompatibleTransport(), new JacksonFactory(), null);

            builder = CloudEndpointUtils.updateBuilder(builder);

            ProductApi endpoint = builder.build();

            CollectionResponseProduct products;

            try {
                products = endpoint.list().execute();
            } catch (IOException e) {
                e.printStackTrace();
                products = null;
            }

                List<Product> _list = products.getItems();
                for (Product product : _list) {
                    HashMap<String, String> item = new HashMap<>();
                    item.put("id", product.getId().toString());
                    item.put("name", product.getName());
                    list.add(item);
                }

            return products;
        }


        protected void onPostExecute() {
            productList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    Intent intent = new Intent(ProductListActivity.this, EditProductActivity.class);

                    HashMap<String, String> hashMap = (HashMap<String, String>) productList.getItemAtPosition(i);

                    long productId = Long.valueOf(hashMap.get("id")).longValue();
                    String productName = hashMap.get("name");

                    Log.i(TAG, String.valueOf(productId));
                    Log.i(TAG, (productName));

                    intent.putExtra("product_id", productId);
                    intent.putExtra("product_name", productName);
                    startActivity(intent);
                }
            });
        }



  }


}


